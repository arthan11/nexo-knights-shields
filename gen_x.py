from PIL import Image, ImageDraw
from random import choice

black = (0,0,0)

img = Image.open('pusta.png')
draw = ImageDraw.Draw(img)
x0, y0 = 115, 111
a = 77

#rec = (x, y+a*11, x+a, y+a*12)
#draw.rectangle(rec, fill=black, outline=black)

code = '10110001100100110100100110111001'
code = ''
for i in range(32):
    code += choice(['1', '0'])


x, y = x0, y0 + a*11
for i, char in enumerate(code):
    #print i, char
    if char == '1':
        rec = (x, y, x+a, y+a)
        draw.rectangle(rec, fill=black, outline=black)
    if i < 11:
        y = y - a
    elif i <= 19:
        x = x + a
    else:
        y = y + a

img.show()